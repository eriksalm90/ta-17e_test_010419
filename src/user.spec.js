const user = require("./user");

describe("user", () => {
 it("user id 1", () => {
  const test1 = user(1);
  expect(test1).toMatchSnapshot();
 });
 it("user id 56", () => {
  const test2 = user(56);
  expect(test2).toMatchSnapshot();
 });
 it("user id 1345", () => {
  const test3 = user(1345);
  expect(test3).toMatchSnapshot();
 });
 it("string input to throw Error", function() {
  expect(function() {
   user("testime");
  }).toThrow("id needs to be integer");
 });
});