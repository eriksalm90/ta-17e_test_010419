function capitalize(word) {
 if (typeof(word)=="string") 
 {
  const firstLetter = word.substr(0, 1);
  const otherLetters = word.substr(1);
  return firstLetter.toUpperCase() + otherLetters;
 }
 else 
 {
  throw new Error(/bad input/);
 }
}

module.exports = capitalize;